"use strict";
 
// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'node-chat';
 
// Port where we'll run the websocket server
var webSocketsServerPort = 8080;
 
// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');
 
/**
 * Global variables
 */
// latest 100 messages
var history = [ ];
// list of currently connected clients (users)
var clients = [ ];
var clientNames = [ ];
 
/**
 * Helper function for escaping input strings
 */
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;')
                      .replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
 
// Array with some colors
var colors = [ 'red', 'green', 'blue', 'magenta', 'purple', 'plum', 'orange' ];
// ... in random order
colors.sort(function(a,b) { return Math.random() > 0.5; } );
 
/**
 * HTTP server
 */
var server = http.createServer(function(request, response) {
    // Not important for us. We're writing WebSocket server, not HTTP server
});
server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});
 
/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. WebSocket request is just
    // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
    httpServer: server
});
 
// This callback function is called every time someone
// tries to connect to the WebSocket server
wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');
 
    // accept connection - you should check 'request.origin' to make sure that
    // client is connecting from your website
    // (http://en.wikipedia.org/wiki/Same_origin_policy)
    var connection = request.accept(null, request.origin); 
    // we need to know client index to remove them on 'close' event
    var index = clients.push(connection) - 1;
    var userName = false;
    var userColor = false;
 
    console.log((new Date()) + ' Connection accepted.');
 
    // send back chat history
    if (history.length > 0) {
        connection.sendUTF(JSON.stringify( { type: 'history', data: history} ));
    }
 
    // user sent some message
    connection.on('message', function(message) {
        if (message.type === 'utf8') { // accept only text
		
			try {
				//var json = JSON.parse(htmlEntities(message.utf8Data));
                                var json = JSON.parse(message.utf8Data);
			} catch (e) {
				console.log('This doesn\'t look like a valid JSON: ', htmlEntities(message.utf8Data));
				return;
			}
		
            if (userName === false) { // first message sent by user is their name
                // remember user name
                userName = json.text;
                clientNames[index] = userName;
                // get random color and send it back to the user
                userColor = colors.shift();
                connection.sendUTF(JSON.stringify({ type:'color', data: userColor }));
                console.log((new Date()) + ' User is known as: ' + userName
                            + ' with ' + userColor + ' color.');
 
            } else { // log and broadcast the message
                console.log((new Date()) + ' Received Message from '
                            + userName + ': ' + json.text + ', to: ' + json.to);
                
                // we want to keep history of all sent messages
                var obj = {
                    time: (new Date()).getTime(),
                    text: json.text,
                    author: userName,
                    color: userColor
                };
                history.push(obj);
                history = history.slice(-100);
 
                var jsonMessage = JSON.stringify({ type:'message', data: obj });
		
                // get mentioned people
                var mentionedPeople = JSON.parse(json.to);
                
                if (mentionedPeople.length === 0) {
                    console.log("Broadcasting message..");
                    for (var i=0; i < clients.length; i++) {
                        clients[i].sendUTF(jsonMessage);
                    }
                } else {
                    var recipients = [ ];
                    recipients.push(connection);
                    
                    for (var i=0; i < mentionedPeople.length; i++) {
                        var clientIndex = clientNames.indexOf(mentionedPeople[i]);
                        if (clientIndex!=-1) {
                            recipients.push(clients[clientIndex]);
                        }
                    }
                    
                    for (var i=0; i < recipients.length; i++) {
                        recipients[i].sendUTF(jsonMessage);
                    }
                }
            }
        }
    });
 
    // user disconnected
    connection.on('close', function(connection) {
        if (userName !== false && userColor !== false) {
            console.log((new Date()) + " Peer "
                + connection.remoteAddress + " disconnected.");
            // remove user from the list of connected clients
            clients.splice(index, 1);
            
            console.log("Removing " + clientNames[index]);
            clientNames.splice(index, 1);
            
            // push back user's color to be reused by another user
            colors.push(userColor);
        }
    });
 
});
